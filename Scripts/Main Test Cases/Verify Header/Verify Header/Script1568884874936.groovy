import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'Verify Free Shipping'
WebUI.openBrowser('', FailureHandling.STOP_ON_FAILURE)

WebUI.authenticate(GlobalVariable.URL_Politix, GlobalVariable.user, GlobalVariable.pass, 5)

WebUI.waitForPageLoad(0)

WebUI.maximizeWindow()

WebUI.verifyElementPresent(findTestObject('Header/a_Free Shipping in Australia'), 0)

'Verify hightline element'
CustomKeywords.'custom.HighlightElement.run'(findTestObject('Header/a_Free Shipping in Australia'))

WebUI.click(findTestObject('Header/a_Free Shipping in Australia'))

WebUI.back()

'Verify phone number'
not_run: WebUI.verifyElementPresent(findTestObject('Header/a_Phone number'), 0)

not_run: CustomKeywords.'custom.HighlightElement.run'(findTestObject('Header/a_Phone number'))

WebUI.verifyElementPresent(findTestObject('Header/span_Need help'), 0)

WebUI.mouseOver(findTestObject('Header/span_Need help'))

not_run: CustomKeywords.'custom.HighlightElement.run'(findTestObject('Header/span_Need help'))

WebUI.verifyElementPresent(findTestObject('Header/a_current country'), 0)

WebUI.verifyElementClickable(findTestObject('Header/i_Account'), FailureHandling.STOP_ON_FAILURE)

WebUI.mouseOver(findTestObject('Header/i_Account'))

WebUI.verifyElementClickable(findTestObject('Header/button_Login'))

WebUI.verifyElementClickable(findTestObject('Header/button_Register'))

WebUI.verifyElementClickable(findTestObject('Header/i_Mini cart'))

WebUI.mouseOver(findTestObject('Header/i_Mini cart'))

CustomKeywords.'custom.HighlightElement.run'(findTestObject('Header/i_Mini cart'))

WebUI.click(findTestObject('Common Test Cases/Primary_Logo'))

'Verify menu category level 1'
WebUI.verifyElementPresent(findTestObject('Header/menu category level 1'), 0)

'Hover on category level 1'
not_run: CustomKeywords.'custom.HighlightElement.run'(findTestObject('Header/menu category level 1'))

'Verify element present'
WebUI.verifyElementPresent(findTestObject('Header/i_Search_icon-search'), 0)

WebUI.click(findTestObject('Header/i_Search_icon-search'))

WebUI.verifyElementPresent(findTestObject('Header/Search form'), 0)

WebUI.click(findTestObject('Header/i_Close search'))

