import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.authenticate(GlobalVariable.URL_Politix, GlobalVariable.user, GlobalVariable.pass, 5)

WebUI.waitForPageLoad(0)

WebUI.maximizeWindow()

WebUI.click(findTestObject('Common Test Cases/span_Login'))

WebUI.setText(findTestObject('Login Page/input_Email Address'), findTestData('Login Page').getValue(2, 7))

WebUI.setText(findTestObject('Login Page/input_Password'), findTestData('Login Page').getValue(2, 8))

WebUI.click(findTestObject('Login Page/button_Sign In'))

'Verify Error message should be displayed as Excel file'
WebUI.verifyElementText(findTestObject('Login Page/span_error_Email'), findTestData('Login Page').getValue(3, 7))

